package me.bbb1991.rp2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {
        String path = ""; // todo get path

//        Files.walkFileTree(path, new FileVisitor());

        List<String> list = getData();

        MrzParser parser = new MrzParser();

        parser.parse(list);

    }

    private static List<String> getData() {

        List<String> lines = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(Main.class.getClassLoader().getResource("input.txt").openStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.trim().startsWith("#")) continue;
                lines.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        return lines;

    }
}
