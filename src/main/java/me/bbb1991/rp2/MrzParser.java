package me.bbb1991.rp2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MrzParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(MrzParser.class);

    private static final String COEFFICIENT = "7317317317<<<3173173<1731731731731731731731";

    private static final String DEFAULT_FORMAT_PATTERN = "#";

    private static final String ROW_SEPARATOR = "===========================================================";

    private static final char[] RUSSIAN_ABC = {' ', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л',
            'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'};

    private static final char[] MACHINE_ABC = {'<', 'A', 'B', 'V', 'G', 'D', 'E', '2', 'J', 'Z', 'I', 'Q', 'K', 'L',
            'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', '3', '4', 'W', 'X', 'Y', '9', '6', '7', '8'};

    private boolean isValid = true;

    private final DecimalFormat numberFormat;
    private final DecimalFormat divisionCodeFormat;
    private final DateFormat extractedDateFormat;
    private final DateFormat printableDateFormat;

    public MrzParser() {
        this.numberFormat = new DecimalFormat(DEFAULT_FORMAT_PATTERN);
        this.divisionCodeFormat = new DecimalFormat(DEFAULT_FORMAT_PATTERN);
        this.extractedDateFormat = new SimpleDateFormat("yyMMdd");
        this.printableDateFormat = new SimpleDateFormat("dd MMMM yy", new Locale("ru", "RU"));
    }

    public void parse(final List<String> list) {
        list.stream()
                .map(e -> e.split("(?<=\\G.{44})"))
                .forEach(e -> {
                    LOGGER.info(ROW_SEPARATOR);
                    LOGGER.info(Arrays.deepToString(e));
                    parseFirstLine(e[0]);
                    parseSecondLine(e[1]);
                });
    }

    /**
     * This method just translates first line (which contains only Surname, First name, Last name) from latin to
     * russian letters
     *
     * @param line parsed first line from MRZ
     */
    private void parseFirstLine(final String line) {

        StringBuilder stringBuffer = new StringBuilder();

        String cleanedLine = line.substring(5);

        for (char currentChar : cleanedLine.toCharArray()) {
            for (int j = 0; j < MACHINE_ABC.length; j++) {
                if (currentChar == MACHINE_ABC[j]) {
                    stringBuffer.append(RUSSIAN_ABC[j]);
                    break;
                }
            }
        }

        LOGGER.info("Parsed full name: {}", stringBuffer);

    }

    private void parseSecondLine(final String line) {
        isValid = true;
        List<Integer> multipliedValues = new ArrayList<>();
        multipliedValues.addAll(extractSeriesAndNumber(line));
        multipliedValues.addAll(extractBirthDate(line));
        // TODO extract sex
        // TODO extract expire date
        multipliedValues.addAll(extractAdditionalInfo(line));

        finalCheck(line, multipliedValues);

        if (isValid) {
            LOGGER.info("All tests successfully passed! Seems like passport original");
        } else {
            LOGGER.warn("Some of tests failed! Seems like passport is FAKE!");
        }
    }

    /**
     * Method, that checks
     *
     * @param line
     * @param multipliedValues
     */
    private void finalCheck(String line, List<Integer> multipliedValues) {
        int controlNumber = Integer.parseInt(line.substring(line.length() - 1));
        int calculatedControlNumber = calculateControlNumber(multipliedValues);

        if (controlNumber == calculatedControlNumber) {
            LOGGER.info("Final checking is OK!");
        } else {
            LOGGER.warn("Final checking is failed! Expected: {}, actual: {}", controlNumber, calculatedControlNumber);
        }
    }

    /**
     * Method, that extracts birth date and control number from MRZ and validates if it is valid or not.
     * For example, if person's birth date is 01.01.1970, then it will be written in MRZ as
     * {@code *************7001011************************}, where is last digit (1) is control number.
     *
     * @param line the second line from MRZ
     * @return {@link List} of multiplied numbers (each number multiplied to coefficient) which might
     * be needed to calculate final checking.
     * @see MrzParser#COEFFICIENT
     */
    private List<Integer> extractBirthDate(final String line) {
        String birthday = line.substring(13, 19);
        try {
            Date date = extractedDateFormat.parse(birthday);
            LOGGER.info("Birthday is: {}", printableDateFormat.format(date));
        } catch (ParseException ex) {
            LOGGER.warn("Cannot parse date: {}!", birthday);
        }
        int controlNumber = Integer.parseInt(line.substring(19, 20));
        return validateControlNumber(controlNumber, birthday, COEFFICIENT.substring(13, 20));
    }

    private List<Integer> extractSeriesAndNumber(final String line) {
        String extractedNumber = (line.substring(0, 9));
        int controlNumber = Integer.valueOf(line.substring(9, 10));

        LOGGER.info("Extracted number is: {}", numberFormat.format(Integer.parseInt(extractedNumber)));

        return validateControlNumber(controlNumber, extractedNumber, COEFFICIENT.substring(0, 10));
    }

    private List<Integer> extractAdditionalInfo(final String line) {

        int lastDigitOfSeries = Integer.parseInt(line.substring(28, 29)); // TODO

        String issueDate = line.substring(29, 35);
        String divisionCode = line.substring(35, 41);
        int controlNumber = Integer.parseInt(line.substring(42, 43));


        try {
            Date date = extractedDateFormat.parse(issueDate);
            LOGGER.info("Date of issue of passport: {}", printableDateFormat.format(date));
        } catch (ParseException e) {
            LOGGER.warn("Cannot parse date {}!", issueDate);
        }

        LOGGER.info("Division code is: {}", divisionCodeFormat.format(Integer.parseInt(divisionCode)));
        return validateControlNumber(controlNumber, line.substring(28, 42).replaceAll("<", "0"), COEFFICIENT.substring(28, 43));
    }

    private List<Integer> validateControlNumber(final int expectedControlNumber, final String number, final String coefficient) {
        List<Integer> multipliedValues = new ArrayList<>();

        for (int i = 0; i < number.length(); i++) {
            multipliedValues.add(Integer.parseInt("" + number.charAt(i)) * Integer.parseInt("" + coefficient.charAt(i)));
        }

        int calculatedControlNumber = calculateControlNumber(multipliedValues);

        if (expectedControlNumber == calculatedControlNumber) {
            LOGGER.info("Control checking passed");
        } else {
            isValid = false;
            LOGGER.warn("Control checking failed! Expected: {}, actual: {}", expectedControlNumber, calculatedControlNumber);
        }

        multipliedValues.add(calculatedControlNumber * Integer.parseInt(coefficient.substring(coefficient.length() - 1)));

        return multipliedValues;
    }

    /**
     * Method for calculating control number from extracted data
     *
     * @param list of numbers (number = extracted number * coefficient)
     * @return calculated control number
     */
    private static int calculateControlNumber(final List<Integer> list) {
        return list.stream().mapToInt(Integer::intValue).sum() % 10;
    }
}
