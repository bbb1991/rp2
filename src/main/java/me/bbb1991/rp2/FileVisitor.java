package me.bbb1991.rp2;

import net.sourceforge.tess4j.Tesseract;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;

public class FileVisitor extends SimpleFileVisitor<Path> {

    private final Tesseract tesseract;

    public FileVisitor() {
        tesseract = new Tesseract();
    }


    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        System.out.printf("Processing file %s...%n", file);

        try {
            String recognizedData = tesseract.doOCR(file.toFile());

            Arrays.stream(recognizedData.split(System.lineSeparator()))
                    .filter(e -> e.contains("<<"))
                    .forEach(System.out::println);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return FileVisitResult.CONTINUE;
    }
}
