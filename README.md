## Description
This script validates MRZ (aka machine readable zone). For now, extracting data from scan image
not working well and it requires to manually enter MRZ by filling file `src/main/resources/input.txt`

## Requirements
* Java 8 (JDK)

To check if JDK installed or not you can enter command `javac -version`. Result should be
something like this: 
```
javac 1.8.0_172
```

## How to run
1. Insert required data to file `src/main/resources/input.txt`
2. Run script by command
```
./mvnw clean compile exec:java
```
